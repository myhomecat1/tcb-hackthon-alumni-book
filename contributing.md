# 共建指南

## 贡献内容主要步骤

1. Fork 此仓库；
2. 根据您要贡献的内容，参考 [您可以贡献的内容](https://blog.csdn.net/cpongo4/article/details/98195803#您可以贡献的内容) 中的说明，在相应模块添加内容。
3. 提交PR。如有必要，请附加详细的内容说明。

## 在提交Pull request之前，请确保：

- 如您的贡献内容引用了本社区的原创内容，请声明并注明出处。
- 您提交的内容不是重复的内容。
- 您提交的内容位于正确的模块、路径下。
- 检查您的语法和书写。
- 提交Pull request时候请附带清楚的描述标题。
- 好好地写commit信息, 仔细阅读每一个CONTRIBUTING文件。
- 不要到处留无用的空格。
- 如果现有的模块不能覆盖您想贡献的内容，欢迎创建新的模块。
- 欢迎您对已存在的共建内容进行改进。
- 确保书写的链接能实现正常跳转。

## Contributing

This project welcomes contributions and suggestions. Most contributions require you to agree to a Contributor License Agreement (CLA) declaring that you have the right to, and actually do, grant us the rights to use your contribution. For details, visit [https://cla.microsoft.com](https://cla.microsoft.com/).

When you submit a pull request, a CLA-bot will automatically determine whether you need to provide a CLA and decorate the PR appropriately (e.g., label, comment). Simply follow the instructions provided by the bot. You will only need to do this once across all repos using our CLA.

This project has adopted the [Microsoft Open Source Code of Conduct](https://opensource.microsoft.com/codeofconduct/). For more information see the [Code of Conduct FAQ](https://opensource.microsoft.com/codeofconduct/faq/) or contact [opencode@microsoft.com](mailto:opencode@microsoft.com) with any additional questions or comments.

# 本社区注明版权出处的内容适用于[License](https://blog.csdn.net/microsoft/ai-edu/blob/master/LICENSE.md)版权许可。