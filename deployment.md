# 📦项目部署说明

### 💽源码下载

1. [git📦仓库](https://gitee.com/hanxu051/tcb-hackthon-alumni-book?_from=gitee_search)
2. ![git仓库.png](https://i.loli.net/2020/04/05/oQdT8CAwLPy29ir.png)

使用✨git进行clone（推荐）或直接下载zip文件（无git基础）

3. ![源码文件.png](https://i.loli.net/2020/04/05/ZsMxEGSQYlK6c85.png)

下载后获得如下文件

4. 在微信小程序开发工具中导入 alumni-book文件夹

![导入.png](https://i.loli.net/2020/04/05/mHL7lXGDqr6pQ3J.png)

5.导入后优先配置云开发环境

- project.config.json文件的27行左右配置自己的appid

- app.js文件的13行左右配置自己的云开发环境名

  - ![云开发环境.png](https://i.loli.net/2020/04/05/3uidKvrDQbPGkyq.png)

- 数据库操作

  - 建表 建立数据库集合message和users
  - users集合中建立索引管理
    - 索引名称 _location
    - 索引属性 非唯一
    - 索引字段 location
    - 升序降序改为 地理位置
  - 权限设置 users集合和message集合都设置为 所有用户可读，仅创建者可读写

- 存储

  - 存储管理
    - 新建文件夹 userPhoto
    - 权限设置 所有用户可读，仅创建者可读写。

- 云函数

  - ![安装依赖.png](https://i.loli.net/2020/04/05/kW8GcRzdNqHXrYg.png)

  - 安装云函数依赖

    - 在云函数cloudfunctions文件夹下选择一个云函数

    - 右键选择在终端中打开

    - ```
      npm install --save wx-server-sdk@latest
      ```

    - 执行上述代码进行安装 wx-servler-sdk依赖

      - 需要提前安装npm [👉npm教程](https://www.runoob.com/nodejs/nodejs-npm.html)
      - 若npm安装依赖失败可尝试使用淘宝镜像cnpm。[👉cnpm教程](https://developer.aliyun.com/mirror/NPM?from=tnpm)
      - ![上传云函数.png](https://i.loli.net/2020/04/05/b6afPxlAtrmv5kZ.png)、
      - 上传云函数，会弹出对应提示。

- 安装vant-ui

  - ```
    npm i @vant/weapp -S --production
    ```

  - 在miniprograme文件夹下用终端打开 运行上述命令，参考上述安装 wx-servler-sdk依赖

5. 🎉至此项目配置工作已经全部完成，开始根据自己的需求进行二次开发吧。

   ## 📧如果有问题请联系

   📧1076998404@qq.com

   