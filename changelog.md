# 📌项目的更新日志
- Initializes the WeChat Mini programingproject
    - 🎉初始化微信小程序项目 
        1. 🎈创建云开发项目
        2. 🗑删除不必要的资源文件
    - 📃创建必要的 
        1. 🔊README.md: 项目的介绍
        2. ⚖LICENSE: 项目的开源许可证
        3. 🔔code-of-conduct.md: 项目的行为准则
        4. 📣contributing.md: 项目的贡献指南
        5. 📣deployment.md: 部署说明
        6. 📦changelog.md: 项目的更新日志
    - 📆2020年4月1日20:13:07