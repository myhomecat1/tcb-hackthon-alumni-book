# tcb-hackthon-alumni-book
# 🎉项目名称
### 📬alumni-book校园通讯录
# ✨项目介绍

**命题方向举例3:校友维系场景**

## 🤦‍♀️项目解决痛点
- 作为一个在校大学生是否遇到过一些事情，有时候我们需要联系学校的一些同学，可能需要找某个社团的部长或是某个专业的同学咨询一个问题，但是关系网复杂，获得同学的联系方式异常困难，更别说联系毕业很久的学长学姐。
- 有可能，我们已经毕业了，招聘时想内推一下学弟学妹，想了解下学校的近况，但是很难获得他们得到联系方式。
- QQ群加了一堆，交友墙里发大喇叭去问，都不尽如人意。
- 开放的互联网平台上又鱼龙混杂，信息广告遍地，急需一个靠谱边界的方式。

#### 👨‍🔧总结 

- 市面上的交友软件花里胡哨广告一大片太烦
- 虚拟的交友软件信息鱼龙混杂，真真假假。
- 校内社交，真实且有隐私。

## 🙋‍♂️项目亮点
- 无广告。本项目希望通过微信小程序，构建一款无广告，真实可靠的校内通讯录。
- 简约。只做最基础功能，不臃肿，主打内容极简，功能简洁直击痛点，不打广告不做推广，只做最朴素的校园通讯录。
- 安全。保护校友的信息安全，隐私内容需要申请后可见。
- 方便。上传自己的个人信息，方便在需要时取得联系。小程序无需下载APP随用随走。
## 🛠技术使用
- 项目使用微信小程序平台进行开发。
- 使用腾讯云开发技术，免费资源配额，无需域名和服务器即可搭建。
- 小程序本身的即用即走，适合小工具的使用场景，也适合程序的开发。

# 🖼项目效果截图
1. 首页![首页.png](https://i.loli.net/2020/04/05/u2rnKxoJDwZaEHF.png)
2. 附近![附近.png](https://i.loli.net/2020/04/05/wT84lE1Z9LP3YmB.png)
3. 消息![消息.png](https://i.loli.net/2020/04/05/VA8npFIPW5cKbmT.png)
4. 我的 ![个人页面.png](https://i.loli.net/2020/04/05/tFf8hIxwKOdCgGr.png)
5. 个人设置 ![修改页面.png](https://i.loli.net/2020/04/05/2bZwC6nDcfalVSx.png)
6. 个人详情
   1. 非好友展示 ![详细信息非好友.png](https://i.loli.net/2020/04/05/V7CbcKlxhwLPyfU.png)
   2. 好友展示 ![详细信息好友.png](https://i.loli.net/2020/04/05/xb1M7YDCnQRt3cS.png)
7. 搜索页面![搜索.png](https://i.loli.net/2020/04/05/WYJwKeLNtVRMZps.png)

# 🎮部署教程
[部署教程文档链接🔗](.\contributing.md)

## 📦项目部署说明

### 💽源码下载

1. #### [git📦仓库](https://gitee.com/hanxu051/tcb-hackthon-alumni-book?_from=gitee_search)

2. ![git仓库.png](https://i.loli.net/2020/04/05/oQdT8CAwLPy29ir.png)

使用✨git进行clone（推荐）或直接下载zip文件（无git基础）

3. ### ![源码文件.png](https://i.loli.net/2020/04/05/ZsMxEGSQYlK6c85.png)

下载后获得如下文件

4. 在微信小程序开发工具中导入 alumni-book文件夹

![导入.png](https://i.loli.net/2020/04/05/mHL7lXGDqr6pQ3J.png)

5.导入后优先配置云开发环境

- #### project.config.json文件的27行左右配置自己的appid

- #### app.js文件的13行左右配置自己的云开发环境名

  - ![云开发环境.png](https://i.loli.net/2020/04/05/3uidKvrDQbPGkyq.png)

- #### 数据库操作

  - 建表 建立数据库集合message和users
  - users集合中建立索引管理
    - 索引名称 _location
    - 索引属性 非唯一
    - 索引字段 location
    - 升序降序改为 地理位置
  - 权限设置 users集合和message集合都设置为 所有用户可读，仅创建者可读写

- #### 存储

  - 存储管理
    - 新建文件夹 userPhoto
    - 权限设置 所有用户可读，仅创建者可读写。

- #### 云函数

  - ![安装依赖.png](https://i.loli.net/2020/04/05/kW8GcRzdNqHXrYg.png)

  - 安装云函数依赖

    - 在云函数cloudfunctions文件夹下选择一个云函数

    - 右键选择在终端中打开

    - ```
      npm install --save wx-server-sdk@latest
      ```

    - 执行上述代码进行安装 wx-servler-sdk依赖

      - 需要提前安装npm [👉npm教程](https://www.runoob.com/nodejs/nodejs-npm.html)
      - 若npm安装依赖失败可尝试使用淘宝镜像cnpm。[👉cnpm教程](https://developer.aliyun.com/mirror/NPM?from=tnpm)
      - ![上传云函数.png](https://i.loli.net/2020/04/05/b6afPxlAtrmv5kZ.png)、
      - 上传云函数，会弹出对应提示。

- #### 安装vant-ui

  - ```
    npm i @vant/weapp -S --production
    ```

  - 在miniprograme文件夹下用终端打开 运行上述命令，参考上述安装 wx-servler-sdk依赖

5. 🎉至此项目配置工作已经全部完成，开始根据自己的需求进行二次开发吧。

   ## 📧如果有问题请联系

   📧1076998404@qq.com

   

# ⚖开源许可证标注
MIT License